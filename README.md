# kenocli

`kenocli>`

```
        //////// 
    //////////////// 
   ////////////////// 
 //                  // 
 //                  // 
///                  ///
///                  ///
 //           //     // 
 //           //     // 
  ////////////////////
     //////////////// 
        ////////

```

Mastodon / Pleroma command line client meant to test the capabilities of the libkenoma library.

This program is in a very early and heavy stage of development, expect many bugs and many changes, we expect to make it usable in the near future though!

This client is being developed along the libkenoma library to test its design and flaws.

# Compile

Clone this repository whith the "--recursive" option to clone the libkenoma submodule, needed for compilation.

In the root directory, run 'make' to compile which will put a binary in the root folder or
'make run' to run.

# Dependencies

- libkenoma (at the moment it is a submodule, will compile with make)
- json-c
- libcurl

# License

© kenocli contributors

Licenced under the GNU General Public License v3.0

Permissions of this strong copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. 
