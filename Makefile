CC = cc
LIBKENOMA_DIR = libkenoma
LIBKENOMA_SO = $(LIBKENOMA_DIR)/build/libkenoma.so
CFLAGS = -lkenoma -ljson-c -lcurl -I./$(LIBKENOMA_DIR)/include -L./$(LIBKENOMA_DIR)/build -Wl,-rpath=./$(LIBKENOMA_DIR)/build -Wall -Wextra
TARGET = kenocli
SRCDIR = src
SOURCES = $(shell find src -type f -iname '*.c')
DEBUG =

all: $(LIBKENOMA_SO) $(TARGET)

$(TARGET): $(SOURCES) 
	#$(MAKE) -C $(LIBKENOMA_DIR)
	$(CC) $(CFLAGS) $(DEBUG) -o $@ $(SOURCES)

.PHONY: $(LIBKENOMA_SO)
$(LIBKENOMA_SO):
	$(MAKE) -C $(LIBKENOMA_DIR) DEBUG="$(DEBUG)"

.PHONY: debug
debug:
	$(MAKE) $(MAKEFILE) DEBUG="-g -o1"

.PHONY: run
run: all
	./$(TARGET)

.PHONY: valgrind
valgrind: all
	valgrind --leak-check=full --show-leak-kinds=definite --track-origins=yes ./$(TARGET)

.PHONY: clean
clean:
	rm -rf $(TARGET)


