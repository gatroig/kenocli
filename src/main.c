#include <stdio.h>

#include "kma_auth.h"
#include "logic/login.h"
#include "logic/prompt.h"


int main(void)
{
    // Clear stdout
    printf("\e[1;1H\e[2J");

    kma_conn *conn = initMenu();

    system("sleep 1");

    printf("\e[1;1H\e[2J");

    if (conn) mainPrompt(conn);

    kma_destroyConn(conn);
}
