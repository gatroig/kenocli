#include "connection.h"

// Free returned string as it is dynamically allocated
static char *get_xdg_path()
{
    // Get from environment variable
    char *based_datadir = getenv("XDG_DATA_HOME");
    if(based_datadir) {
        // TODO: Memory error check
        based_datadir = strdup(based_datadir);
    }
    // If that doesn't help, assemble it ourselves
    else {
        char *homedir = getenv("HOME");
        if(homedir) {
            // TODO: memory error check
            asprintf(&based_datadir, "%s/%s", homedir, DEFAULT_DATA_DIR);
        }
        else {
            // TODO
            fprintf(stderr, "Home directory wasn't found. Don't use Windows.");
            exit(1);
        }
    }

    // Now we have to append our application name as a subdir to the base path
    // TODO: memory error check
    char *datadir = NULL;
    asprintf(&datadir, "%s/kenocli", based_datadir);
    free(based_datadir);

    return datadir;
}

void saveConnection(kma_conn *conn, char *filename)
{
    FILE *saved_conn;

    char *datadir = get_xdg_path();

    mkdir(datadir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    // open file for writing
    char *filepath = NULL;
    // TODO: memory error check
    asprintf(&filepath, "%s/%s%s", datadir, filename, DEFAULT_CONN_EXT);

    puts(filepath);

    saved_conn = fopen(filepath, "w");
    if (!saved_conn) {
        fprintf(stderr, "\nError opening file\n");
        exit (EXIT_FAILURE);
    }

    // write strings to file
    int test = 1;
    test *= putw(strlen(conn->client_id), saved_conn) != EOF;
    test *= fwrite(conn->client_id, sizeof(char), strlen(conn->client_id), saved_conn);
    test *= putw(strlen(conn->client_secret), saved_conn) != EOF;
    test *= fwrite(conn->client_secret, sizeof(char), strlen(conn->client_secret), saved_conn);
    test *= putw(strlen(conn->instance), saved_conn) != EOF;
    test *= fwrite(conn->instance, sizeof(char), strlen(conn->instance), saved_conn);
    test *= putw(strlen(conn->app_token), saved_conn) != EOF;
    test *= fwrite(conn->app_token, sizeof(char), strlen(conn->app_token), saved_conn);
    test *= putw(strlen(conn->user_token), saved_conn) != EOF;
    test *= fwrite(conn->user_token, sizeof(char), strlen(conn->user_token), saved_conn);

    if (test != 0)
        printf("Contents to file written successfully !\n");
    else
        printf("Error writing file !\n");

    // close file
    fclose(saved_conn);
    free(datadir);
    free(filepath);
}

kma_conn *loadConnection(char *filename)
{
    char *absolute_dir = get_xdg_path();
    char *absolute_path = NULL;
    asprintf(&absolute_path, "%s/%s", absolute_dir, filename);
    free(absolute_dir);

    FILE *saved_conn = fopen(absolute_path, "r");
    free(absolute_path);

    if (!saved_conn) {
        fputs("File not found", stderr);
    }

    int test = 1;
    char *strings[5];
    for (int i = 0; i < 5; i++) {
        int size = 0;
        fread(&size, sizeof(int), 1, saved_conn);
        strings[i] = malloc(sizeof(char*) * size);
        test *= fread(strings[i], sizeof(char), size, saved_conn);
    }

    if (test != 0)
        printf("Connection loaded from disk successfully!\n");
    else
        printf("Error loading file !\n");

    kma_conn *conn = kma_recreateConn(strings[0], strings[1], strings[2], strings[3], strings[4]);

    for (int i = 0; i < 5; i++) {
        if (strings[i]) free(strings[i]);
    }

    return conn;
}

// TODO: Maybe rename listConnections function to getConnectionList?
void destroyListConnections(char** connectionList){
    if (connectionList) {
        for (int i = 0; connectionList[i]; i++) {
            free(connectionList[i]);
        }
        free(connectionList);
    }
}

char **listConnections()
{
    int arrsize = 2;
    char** namearray = malloc(sizeof(char*) * arrsize);
    int i = 0;

    char *ext = DEFAULT_CONN_EXT;
    char *directory = get_xdg_path();
    DIR *dir;
    struct dirent *dirstruct;
    dir = opendir(directory);
    free(directory);
    if (!dir) {
        fputs("Error getting connections folder!", stderr);
        return NULL;
    }

    while ((dirstruct = readdir(dir)) != NULL) {
        // Skip files that don't end in .conn
        int ext_offset = strlen(dirstruct->d_name) - strlen(ext);
        if(strcmp(&dirstruct->d_name[ext_offset], DEFAULT_CONN_EXT) != 0) continue;

        // Make array bigger if it needs to be
        // Additional +1 to make room for additional NULL
        if(i+1+1 > arrsize) {
            arrsize *= 2;
            namearray = realloc(namearray, sizeof(char*)*arrsize);
        }

        namearray[i] = strdup(dirstruct->d_name);

        i++;
    }

    closedir(dir);

    // NULL terminator
    namearray[i] = NULL;

    return namearray;
}
