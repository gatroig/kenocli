#ifndef TEMPORARY_H_
#define TEMPORARY_H_

#include <stdlib.h>

#include "kma_status.h"

#define FILE_TEMPLATE_TL "keno-tl-XXXXXX"
#define FILE_TEMPLATE_POST "keno-post-XXXXXX"

void pagerTL(kma_status **statuses);

char *postEditor();

#endif // TEMPORARY_H_
