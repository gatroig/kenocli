// Saving to disk functions
#ifndef PERS_CONNECTION_H_
#define PERS_CONNECTION_H_

// FIXME: DELETE GNU FROM existence WHERE 1 = 1
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>

#include "kma_auth.h"

#define DEFAULT_CONN_EXT ".conn"
#define DEFAULT_DATA_DIR ".local/share"

// Saves connection given a filename
void saveConnection(kma_conn *conn, char *filename);
// Gets a saved connection from secondary memory
kma_conn *loadConnection(char* filename);
// Returns array of all saved connection names
char **listConnections();
void destroyListConnections(char **connectionList);

#endif // PERS_CONNECTION_H_
