#include "login.h"

#include <string.h>

kma_conn *initMenu()
{
    // Get dinamically stored array of *.conn strings
    char **saved_connections = listConnections();
    int num_of_conns = 0;
    // Print list and additional info
    if (!saved_connections) {
        fputs("Error retrieving saved connections!", stderr);
    }
    else {
        // conn list is NULL terminated. Iterate until finding NULL.
        // if first place is null, no connection is there.
        if (saved_connections[0])
            puts("Connections detected! Insert a connection number.\n");
        else
            puts("No connections detected.\n");

        // rpint "i: filename.conn" loop
        for(int i = 0; saved_connections[i]; i++) {
            printf("%d: %s\n\n", i+1, saved_connections[i]);
            num_of_conns++;
        }
    }

    puts("To create a new connection, type 'n'");
    puts("To exit the program, type 'q'");

    char selection[32];
    kma_conn *conn = NULL;
    while(!conn) {
        printf("> ");
        // %31s to limit the string length to 31
        scanf("%31s", (char *)&selection);

        if (feof(stdin)) {
            clearerr(stdin);
            strcpy(selection, "q");
        }

        switch(selection[0]) {
            case 'n':
                conn = createConnection();
                puts("Input connection name:");
                char filename[32];
                scanf("%s", filename);
                puts("Saving to disk... ");
                saveConnection(conn, filename);
                break;
            case 'q':
                exit(EXIT_SUCCESS);
                break;
            default:
                // Not an accepted letter, test number
                int selnum = atoi(selection);
                if(selnum <= 0)
                    puts("Command not recognized. Please try again:\n");
                else if(selnum > num_of_conns)
                    puts("Number does not correspond with available connections, try again:\n");
                else
                    conn = loadConnection(saved_connections[selnum-1]);
                break;
        }
    }

    destroyListConnections(saved_connections);

    puts("Connected successfully!\n");
    return conn;
}

kma_conn *createConnection()
{
    // Constants
    const char *default_scope = "read+write+follow";
    const char *default_redir = "urn:ietf:wg:oauth:2.0:oob";
    char *prompt = "newconn> ";

    // Assembling URL, protocol + instance
    char *instance = NULL;
    {
        puts("Input instance name:");
        printf("%s",prompt);
        char *protocol = "https://";
        int protlen = strlen(protocol);
        char buf[128];
        instance = buf;
        scanf("%s", &instance[protlen]);
        if (strncmp(protocol, &instance[protlen], protlen) == 0) {
            instance += protlen;
        }
        else {
            strncpy(instance, protocol, protlen);
        }
        puts("");
    }

    kma_conn *conn = kma_createConn(instance, default_scope, default_redir, "kenocli", "https://kenoma.cum");

    if (!conn) {
        puts("Instance may not be correct");
        puts("Could not create connection, try again...\n");
        return NULL;
    }

    // URL auth
    char *auth_url = kma_getAuthURL(conn, default_scope, default_redir, KMA_FORCE_LOGIN);

    puts("Open this link and paste the code: ");
    printf("\"%s\"\n", auth_url);
    free(auth_url);

    char code[256];
    puts("Input code:");
    printf("%s", prompt);

    while (scanf("%s", code) != 1) {
        fprintf(stderr, "Please input *something*\n");
    }

    puts("Code inputed\n");

    kma_getUserToken(conn, default_scope, default_redir, code);

    if (conn->user_token == NULL) {
        fputs("Code incorrect, exiting ...", stderr);
        exit(EXIT_FAILURE);
    }

    if (kma_verifyUser(conn)) {
        puts("Creation successful!");
    }

    else {
        puts("Creation unsuccessful, couldn't verify connection");
    }

    return conn;
}
