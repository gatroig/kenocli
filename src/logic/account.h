#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include <stdio.h>

#include "kma_account.h"
#include "kma_status.h"

void printAccount(kma_account *account);

#endif // ACCOUNT_H_
