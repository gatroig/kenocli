#include "prompt.h"
#include "kma_status.h"
#include "kma_notif.h"

static void printWelcome()
{
    puts(
         "        ////////        \n"
         "    ////////////////    \n"
         "   //////////////////   \n"
         " //                  // \n"
         " //                  // \n"
         "///                  ///\n"
         "///                  ///\n"
         " //           //     // \n"
         " //           //     // \n"
         "  ////////////////////  \n"
         "     ////////////////   \n"
         "        ////////        \n"
         "      | kenocli> |      \n"
    );

    puts("Type ':help' for help");
}

void mainPrompt(kma_conn *conn)
{
    kma_status **timeline_buffer = NULL;
    int limit = KMA_DEFAULT_TL_LIMIT;

    printWelcome();
    uint8_t quit = 0;
    while (!quit) {
        printf("> ");
        char typed[32];
        scanf("%s", typed);

        {
            int c;
            while ((c = getchar()) != '\n' && c != EOF) { }
        }

        puts("");

        int number = atoi(typed);

        if (feof(stdin)) {
            clearerr(stdin);
            strcpy(typed, ":exit");
        }

        if (strcmp(typed, ":post") == 0 ||
            strcmp(typed, ":p") == 0
        ) {
            postSurgery(conn, NULL);
        }
        else if (strcmp(typed, ":tlpub") == 0) {
            if (timeline_buffer) kma_destroyTL(timeline_buffer);
            timeline_buffer = printPublicTL(conn, limit);
        }
        else if (strcmp(typed, ":profile") == 0) {
            //printAccount()
        }
        else if (
            strcmp(typed, ":tlhome") == 0 ||
            strcmp(typed, ":tl") == 0
        ) {
            if (timeline_buffer) kma_destroyTL(timeline_buffer);
            timeline_buffer = printHomeTL(conn, limit);
        }
        else if (strcmp(typed, ":notif") == 0) {
            kma_notif **notifs = printNotifArray(conn, limit);
        }
        else if (strcmp(typed, ":exit") == 0 ||
            (strcmp(typed, ":quit") == 0) ||
            (strcmp(typed, ":q") == 0)
        ) {
            puts("Exit? (y/n)");
            char selection = fgetc(stdin);

            if (feof(stdin)) {
                clearerr(stdin);
                selection = 'y';
            }

            if (selection && selection == 'y') {
                if (timeline_buffer) kma_destroyTL(timeline_buffer);
                puts("bye...");
                quit = 1;
            }
            else puts("");
        }
        else if (strcmp(typed, ":limit") == 0) {
            puts("Input a number to limit the number of posts seen (between 1 and 75):");
            (scanf("%d", &limit));
            while((limit < 1 || limit > 75)) {
                puts("please input a correct value");
                (scanf("%d", &limit));
                if (feof(stdin)) clearerr(stdin);
            }
            printf("Limit updated to %d\n", limit);
        }
        else if (strcmp(typed, ":help") == 0) {
            puts(
                "Commands:\n"
                " - ':post' : Enter post composer mode\n"
                " - ':tlpub' : Print public TL\n"
                " - ':tlhome' or ':tl' : Print home TL\n"
                " - [post number shown on tl] : interact with post\n"
                " - ':exit' or ':quit' or ':q' : Quit program\n"
                " - ':clear' : Clear the prompt screen\n"
                " - ':help' : Print this help\n"
                " - ':limit' : Establish the limit of posts shown on the timeline\n"
                " - ':profile' : Diplay your account info\n"
                "\n"
                );
        }
        else if (strcmp(typed, ":clear") == 0) {
            // Clear the screen
            printf("\e[1;1H\e[2J");
            printWelcome();
        }
        else if ((number > 0) && (number < limit)) {
            // TIMELINE ACTIONS
            {
                if (!timeline_buffer) {
                    puts("Please get a timeline before selecting a post");
                }
                else {
                    char *id = timeline_buffer[number - 1]->id;
                    char action[32];
                    uint8_t quit = 0;
                    puts("Available actions: \n"
                            "- ':reply' : Reply to the selected post\n"
                            "- ':fav' : Favourite the selected post\n"
                            "- ':reblog' : Reblog the selected post\n"
                            "- ':profile' : Display poster account\n"
                            "- ':exit' : Return to the main prompt\n"
                    );
                    puts("Current post:");
                    printStatus(timeline_buffer[number - 1]);
                    while (!quit) {
                        printf("status %d> ", number);
                        scanf("%s", action);

                        {
                            int c;
                            while ((c = getchar()) != '\n' && c != EOF) { }
                        }

                        if (strcmp(action, ":reply") == 0) {
                            postSurgery(conn, id);
                            quit = 1;
                        }
                        else if (strcmp(action, ":fav") == 0) {
                            kma_status *fav_status = kma_favStatus(conn, id);
                            kma_destroyStatus(fav_status);
                            //quit = 1;
                        }
                        else if (strcmp(action, ":reblog") == 0) {
                            kma_status *reblog_status = kma_reblogStatus(conn, id);
                            kma_destroyStatus(reblog_status);
                            //quit = 1;
                        }
                        else if (strcmp(action, ":profile") == 0) {
                            printAccount(timeline_buffer[number - 1]->account);
                            //quit = 1;
                        }
                        else if(strcmp(action, ":exit") == 0) {
                            quit = 1;
                        }
                        else puts("Command is not an available action");

                    }
                }
            }

        }


        else {
            puts("Unrecognized command. Type ':help' for available commands");
        }

    }
}
