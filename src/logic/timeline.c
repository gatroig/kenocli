#include "timeline.h"

kma_status **printPublicTL(kma_conn *conn, int limit)
{
    kma_status **statuses = kma_getTL(conn,
                                      0, 0, 0,
                                      NULL, NULL, NULL,
                                      limit,
                                      KMA_PUBLIC_TL
    );

    for (int i = 0; statuses[i] != NULL; i++) {
        printf("\033[31;1;4m%d:\n\033[0m", (i + 1));
        printStatus(statuses[i]);
    }

    // To implement replying
    return statuses;
}

kma_status **printHomeTL(kma_conn *conn, int limit)
{
    kma_status **statuses = kma_getTL(conn,
                                      0, 0, 0,
                                      NULL, NULL, NULL,
                                      limit,
                                      KMA_HOME_TL
    );

    for (int i = 0; statuses[i] != NULL; i++) {
        printf("\033[31;1;4m%d>\n\033[0m", (i + 1));
        printStatus(statuses[i]);
    }

    // To implement replying
    return statuses;
}

void printAttachments(kma_media **media_array)
{
    for (int i = 0; media_array[i] != NULL; i++) {
        printf("---Attachment %d---\n", i + 1);
        printf("Type: %d\n", media_array[i]->type);
        puts(media_array[i]->text_url);
        if (media_array[i]->description)
            printf("Description: %s", media_array[i]->description);
    }
}

void printStatus(kma_status *status)
{
    if (status == NULL) return;
    printf("| ");
    printf("%-15s | @%-15s\n", status->account->display_name, status->account->acct);
    printf("|----------------------------------------->\n");
    kma_statusFormat(status);
    printf("%s\n", status->content);
    printAttachments(status->media_attachments);
    printf("\n|----------------------------------------->\n");
    printf("|     Reshares: %-3d | Favourites: %-3d     > \n",
            status->reblogs_count, status->favourites_count);
    printf("|----------------------------------------->\n\n");
}

