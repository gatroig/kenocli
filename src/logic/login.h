#ifndef LOGIN_H_
#define LOGIN_H_

#include <stdio.h>
#include <stdlib.h>

#include "../persistence/connection.h"
#include "kma_auth.h"


kma_conn *initMenu();

kma_conn *createConnection();

#endif // LOGIN_H_
