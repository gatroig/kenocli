#include "notifications.h"

void printNotif(kma_notif *notif)
{
    if (notif == NULL) return;
    if (notif->account) {
        printf("From: %s | %s\n", notif->account->display_name, notif->account->acct);
    }
    // TODO Type
    printf("Type: TODO\n");
    if (notif->status) {
        printStatus(notif->status);
    }

}

kma_notif **printNotifArray(kma_conn *conn, int limit)
{
    kma_notif **notifs = kma_getNotifs(conn,
                              NULL, NULL,
                              limit,
                              NULL,
                              NULL
    );

    for (int i = 0; notifs[i] != NULL; i++) {
        printf("\033[34;1;4m%d:\n\033[0m", (i + 1));
        printNotif(notifs[i]);
    }

    // To implement replying
    return notifs;
}
