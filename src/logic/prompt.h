#ifndef PROMPT_H_
#define PROMPT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "kma_auth.h"
#include "kma_status.h"

#include "timeline.h"
#include "post.h"
#include "account.h"
#include "notifications.h"

void mainPrompt(kma_conn *conn);

#endif // PROMPT_H_
