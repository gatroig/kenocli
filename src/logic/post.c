#include "post.h"

// they did surgery on a post
void postSurgery(kma_conn *conn, char *replied_id)
{
    int text_s = 16;
    char *text = malloc(sizeof(char) * text_s);
    text[0] = '\0';

    int spoiler_s = 16;
    char *spoiler_text = malloc(sizeof(char) * spoiler_s);
    spoiler_text[0] = '\0';

    uint8_t sensitive = 0;

    enum visibility vis = PUBLIC;

    uint8_t quit = 0;
    while (!quit) {

        puts("----------");
        if (replied_id) printf("In reply to: %s\n", replied_id);
        if (strlen(spoiler_text) > 0) printf("\nCurrent spoiler:\n%s\n\n", spoiler_text);
        if (strlen(text) > 0) printf("\nCurrent text:\n%s\n\n", text);
        switch (vis) {
            case PUBLIC:
                puts("Public visibility");
                break;
            case UNLISTED:
                puts("Unlisted visibility");
                break;
            case PRIVATE:
                puts("Private visibility");
                break;
            case DIRECT:
                puts("Direct message");
                break;
        }
        puts("----------\n");

        printf("post> ");
        char typed[32];
        int c;

        scanf("%31s", typed);
        while ((c = getchar()) != '\n' && c != EOF);
        puts("");

        if (feof(stdin)) {
            clearerr(stdin);
            strcpy(typed, ":exit");
        }

        if (strcmp(typed, ":text") == 0) {
            puts("Type your message here. Press CTRL-D to finish.\n");
            // Read one line at a time, concatenate
            int input_c;
            int i;
            for(i = 0; (input_c = getchar()) != EOF; i++) {
                // We need a slot for '\0'
                if ((i + 1 + 1) >= text_s) {
                    text_s *= 2;
                    text = realloc(text, sizeof(char) * text_s);
                }
         text[i] = input_c;
            }
            clearerr(stdin);
            text[i] = '\0';
            // After EOF it's necessary to use this function to recieve further input

        }
        else if (strcmp(typed, ":spoiler") == 0) {
            puts("Type your message here. Press CTRL-D to finish.\n");
            // Read one line at a time, concatenate
            int input_c;
            int i;
            for(i = 0; (input_c = getchar()) != EOF; i++) {
                // We need a slot for '\0'
                if ((i + 1 + 1) >= spoiler_s) {
                    spoiler_s *= 2;
                    spoiler_text = realloc(spoiler_text, sizeof(char) * spoiler_s);
                }
                spoiler_text[i] = input_c;
            }
            clearerr(stdin);
            spoiler_text[i] = '\0';


        }
        else if (strcmp(typed, ":textclear") == 0) {
            strcpy(text, "");
            text_s = 16;
            text = realloc(text, sizeof(char) * text_s);
            puts("Text removed");
            strcpy(spoiler_text, "");
            spoiler_s = 16;
            spoiler_text = realloc(spoiler_text, sizeof(char) * spoiler_s);
            puts("Spoiler text removed");
            puts("");
        }
        else if (strcmp(typed, ":post") == 0) {
            if (text && strlen(text) > 0) {
                kma_status *status = NULL;
                status = kma_postStatus(conn,
                                        text,
                                        replied_id,
                                        sensitive,
                                        spoiler_text,
                                        vis
                );
                if (status) kma_destroyStatus(status);
                else puts("Error posting");
                quit = 1;
            }
            else puts("Please add something to the post first\n");
        }
        else if (strcmp(typed, ":public") == 0) vis = PUBLIC;
        else if (strcmp(typed, ":unlisted") == 0) vis = UNLISTED;
        else if (strcmp(typed, ":private") == 0) vis = PRIVATE;
        else if (strcmp(typed, ":direct") == 0) vis = DIRECT;
        else if(strcmp(typed, ":help") == 0) {
            puts(
                "Commands(post composer mode):\n"
                " - ':post' : Finish composing and post status\n"
                " - ':text' : Start composing with text\n"
                " - ':textclear' : Reset composed text\n"
                " - ':spoiler' : Add spoiler text\n"
                " - ':public' : Change visibility to public\n"
                " - ':unlisted' : Change visibility to unlisted\n"
                " - ':private' : Change visibility to private\n"
                " - ':direct' : Change to direct message (put @[username] at the beggining of the message)\n"
                " - ':exit' or ':quit' or ':q' : Return to main prompt\n"
                " - ':help' : Print this help"
                "\n"
                );
        }
        else if ((strcmp(typed, ":exit") == 0) ||
            (strcmp(typed, ":quit") == 0) ||
            (strcmp(typed, ":q") == 0)) {
            quit = 1;
        }
        else {
            puts("Unrecognized command. Type ':help' for available commands");
        }

    }

    if (text) free(text);
    if (spoiler_text) free(spoiler_text);

}
