#ifndef POST_H_
#define POST_H_

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "kma_auth.h"
#include "kma_status.h"

void postSurgery(kma_conn *conn, char *relpied_id);

#endif // POST_H_
