#ifndef NOTIFICATIONS_H_
#define NOTIFICATIONS_H_

#include <kma_auth.h>
#include <kma_notif.h>

#include "timeline.h"

void printNotif(kma_notif *notif);
kma_notif **printNotifArray(kma_conn *conn, int limit);

#endif // NOTIFICATIONS_H_
