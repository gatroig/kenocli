#include "account.h"

void printAccount(kma_account *account)
{
    puts("");
    printf("\t%s | %s\n", account->display_name, account->acct);
    printf("\tFollowers: %-4d | Following: %-4d\n",
           account->followers_count, account->following_count);
    puts("");
}
