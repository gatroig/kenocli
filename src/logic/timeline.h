#ifndef LOGIC_TIMELINE_H_
#define LOGIC_TIMELINE_H_

#include <kma_auth.h>
#include <kma_timeline.h>
#include <stdio.h>

kma_status **printPublicTL(kma_conn *conn, int limit);
kma_status **printHomeTL(kma_conn *conn, int limit);
void printStatus(kma_status *status);
#endif //LOGIC_TIMELINE_H_
